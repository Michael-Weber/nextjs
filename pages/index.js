import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>xai.vercel.app</title>
        <meta name="description" content="xai.vercel.app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          <a>Welcome to xai.vercel.app</a>
        </h1>
            <p>
              We are updating this site. Visit our Blog!
              <p><span style="font-size:24px"><strong><a href="https://vvv.code.blog" target="_blank">vvv.code.blog</a></strong></span></p>
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
          Copyright © 2022 xai.vercel.app
        </a>
      </footer>
    </div>
  )
}
